---
title: "About"
image: "profile.jpg"
weight: 1
---

Neoantigen vaccines are novel cancer therapies that enable the immune system’s cells to target and eliminate tumor cells. They are tumor specific molecules that are presented on the surface of the cancer cell and allow identifying it as a mutated cell. Providing the body with those patient specific “cheat sheets” drives a strong T-cell response and gives it the edge it needs to win the fight against the tumor.

With our technology we are aiming to solve two key shortcomings of the current cancer therapeutic landscape:
- A cost effective low-volume/high-mix manufacturing system for the production of neoantigens.
- A platform for the effective delivery of the neoantigen sequences to immune cells, providing optimal activation of the immune system and specific targeting of cancerous cells.

