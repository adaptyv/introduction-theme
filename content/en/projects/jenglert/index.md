---
title: "Julian Englert"
weight: 3
resources:
    - src: camera.jpg
      params:
          weight: -1
---
## **Business Development**

**Past**

MSc Materials Science & Engineering (EPFL, Switzerland)

BSc Materials Science & Engineering (Karlsruhe Institute of Technology, Germany)
