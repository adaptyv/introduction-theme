---
title: "Daniel Gutierrez"
weight: 2
resources:
    - src: worksday.jpg
      params:
          weight: -1
---
## **Immunoengineering**

**Past**

MSc Bioengineering (EPFL, Switzerland)

BSc Biomedical Engineering (University of Leeds, England)
