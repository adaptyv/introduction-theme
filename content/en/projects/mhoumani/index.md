---
title: "Moustafa Houmani"
weight: 1
resources:
    - src: profile.jpg
      params:
          weight: -1
---
## **Manufacturing**

**Past**

MSc Bioengineering (EPFL, Switzerland)

BSc Chemical Engineering (American University of Beirut, Lebanon)